const Review = require('../models/reviewModels');
const catchAsync = require('../utils/catchAsync');
const factory = require('./handlerFactory');

// exports.getAllReviews = factory.getAll(Review);
exports.getAllReviews = async (req, res, next) => {
  let filter = {};
// Hum isme ek hack khel rahe jake factory handler getall me dekho
  if (req.params.tourId) filter = { tour: req.params.tourId };
  const reviews = await Review.find(filter);
  res.status(200).json({
    status: 'success',
    results: reviews.length,
    data: {
      reviews,
    },
  });
};
exports.setTourUserId = (req, res, next) => {
  if (!req.body.tour) req.body.tour = req.params.tourId;
  if (!req.body.user) req.body.user = req.user.id;
  next();
};
exports.getReview = factory.getOne(Review);
//Hame isme create wala factoryhandler ka function use karna isiliye hum isme ek middleware jod de rahe taki tourid or userid mil jaye
// exports.createReview = factory.createOne(Review);
// exports.deleteReview = factory.deleteOne(Review);


exports.deleteReview =  async (req, res, next) => {
    const doc = await Review.findByIdAndDelete(req.params.id);
   
    if (!doc) {
      return new AppError('No doc found with that id', 404);
    }
    res.status(200).json({
      status: 'success',
      data: {
        doc,
      },
    });
  };

// exports.updateReview = factory.updateOne(Review);
exports.updateReview = async (req, res, next) => {
    const doc = await Review.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
    
    if (!doc) {
      return next(new AppError('No document found with that id', 404));
    }
    res.status(200).json({
      status: 'success',
      data: {
        data: doc,
      },
    });
  };

exports.createReview = async (req, res, next) => {
  const newReview = await Review.create(req.body);
  
  res.status(200).json({
    status: 'success',
    data: {
      review: newReview,
    },
  });
};
