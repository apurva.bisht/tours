const request = require('supertest');
const app = require("../app")

const AppError = require('../utils/appError');
const tourController = require('../controller/tourcontroller');
const Tour = require("../models/tourModels");
const createmockdata = require("../tests/mock-data/tour-data.json");
const updatemockdata = require("../tests/mock-data/update-data.json")


const mockGetTourDetails = 
{
  status: 'success',
  data: {
    tour: {
      startLocation: [Object],
      ratingsAverage: 4.8,
      ratingsQuantity: 6,
      images: [Array],
      startDates: [Array],
      secretTour: false,
      guides: [Array],
      _id: '5c88fa8cf4afda39709c2955',
      name: 'test tour',
      duration: 7,
      maxGroupSize: 15,
      difficulty: 'medium',
      price: 497,
      summary: 'Exploring the jaw-dropping US east coast by foot and by boat',
      description: 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n' +
        'Irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
      imageCover: 'tour-2-cover.jpg',
      locations: [Array],
      slug: 'the-sea-explorer',
      __v: 0,
      durationWeeks: 1,
      reviews: [Array],
      id: '5c88fa8cf4afda39709c2955'
    }
  }
}

const mockRequest = () => {
    const req = {}
    req.body = jest.fn().mockReturnValue(req)
    req.params = jest.fn().mockReturnValue(req)
    return req
  }

const mockResponse = () => {
  const res = {}
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res
}

describe('shows the details of tour by tour id', () => {
  jest.setTimeout(100000000);

  it("should return data of tour when id param is provided", async () => {
    const req = mockRequest();
    const res = mockResponse() 
    req.params.id = "5c88fa8cf4afda39709c2955";

    Tour.findById = jest.fn().mockResolvedValue(mockGetTourDetails) 

    let tour =  await tourController.getTour(req,res)
    expect(Tour.findById).toHaveBeenCalledTimes(1)
    expect(res.status).toHaveBeenCalledWith(200);
  });

  it('should not return data of tour when id param is provided wrong', async () => {
    const req = mockRequest();
    const res = mockResponse()

    req.params.id = "5c88fa8cf4afda39709c2956";

    Tour.findById = jest.fn().mockResolvedValue(null) 
    await tourController.getTour(req,res)
    let error = new AppError('No tour found with that id', 404);

    expect(error.statusCode).toBe(404);
    expect(error.status).toBe('fail');
  });

});

const mockGetAllTours = {
  "status": "success",
  "results": 10,
  "data": {
      "tours": [
          {
              "startLocation": {
                  "type": "Point",
                  "description": "Miami, USA",
                  "coordinates": [
                      -80.185942,
                      25.774772
                  ],
                  "address": "301 Biscayne Blvd, Miami, FL 33132, USA"
              },
              "ratingsAverage": 4.8,
              "ratingsQuantity": 6,
              "images": [
                  "tour-2-1.jpg",
                  "tour-2-2.jpg",
                  "tour-2-3.jpg"
              ],
              "startDates": [
                  "2021-06-19T09:00:00.000Z",
                  "2021-07-20T09:00:00.000Z",
                  "2021-08-18T09:00:00.000Z"
              ],
              "secretTour": false,
              "guides": [
                  {
                      "role": "lead-guide",
                      "_id": "5c8a22c62f8fb814b56fa18b",
                      "name": "Miyah Myles",
                      "email": "miyah@example.com",
                      "photo": "user-12.jpg"
                  },
                  {
                      "role": "guide",
                      "_id": "5c8a1f4e2f8fb814b56fa185",
                      "name": "Jennifer Hardy",
                      "email": "jennifer@example.com",
                      "photo": "user-6.jpg"
                  }
              ],
              "_id": "6100ef26a5630831e4aa1cf7",
              "name": "Test tour 1",
              "duration": 7,
              "maxGroupSize": 15,
              "difficulty": "medium",
              "price": 497,
              "summary": "Exploring the jaw-dropping US east coast by foot and by boat",
              "description": "Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\nIrure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
              "imageCover": "tour-2-cover.jpg",
              "locations": [
                  {
                      "type": "Point",
                      "coordinates": [
                          -80.128473,
                          25.781842
                      ],
                      "_id": "5c88fa8cf4afda39709c2959",
                      "day": 1
                  },
                  {
                      "type": "Point",
                      "coordinates": [
                          -80.647885,
                          24.909047
                      ],
                      "_id": "5c88fa8cf4afda39709c2958",
                      "day": 2
                  },
                  {
                      "type": "Point",
                      "coordinates": [
                          -81.0784,
                          24.707496
                      ],
                      "_id": "5c88fa8cf4afda39709c2957",
                      "day": 3
                  },
                  {
                      "type": "Point",
                      "coordinates": [
                          -81.768719,
                          24.552242
                      ],
                      "_id": "5c88fa8cf4afda39709c2956",
                      "day": 5
                  }
              ],
              "slug": "test-tour-1",
              "durationWeeks": 1,
              "id": "6100ef26a5630831e4aa1cf7"
          },
          {
              "startLocation": {
                  "type": "Point",
                  "description": "California, USA",
                  "coordinates": [
                      -122.29286,
                      38.294065
                  ],
                  "address": "560 Jefferson St, Napa, CA 94559, USA"
              },
              "ratingsAverage": 4.4,
              "ratingsQuantity": 7,
              "images": [
                  "tour-7-1.jpg",
                  "tour-7-2.jpg",
                  "tour-7-3.jpg"
              ],
              "startDates": [
                  "2021-02-12T10:00:00.000Z",
                  "2021-04-14T09:00:00.000Z",
                  "2021-09-01T09:00:00.000Z"
              ],
              "secretTour": false,
              "guides": [
                  {
                      "role": "lead-guide",
                      "_id": "5c8a22c62f8fb814b56fa18b",
                      "name": "Miyah Myles",
                      "email": "miyah@example.com",
                      "photo": "user-12.jpg"
                  },
                  {
                      "role": "guide",
                      "_id": "5c8a23412f8fb814b56fa18c",
                      "name": "Ben Hadley",
                      "email": "ben@example.com",
                      "photo": "user-13.jpg"
                  }
              ],
              "_id": "5c88fa8cf4afda39709c296c",
              "name": "The Wine Taster",
              "duration": 5,
              "maxGroupSize": 8,
              "difficulty": "easy",
              "price": 1997,
              "summary": "Exquisite wines, scenic views, exclusive barrel tastings,  and much more",
              "description": "Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\nIrure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
              "imageCover": "tour-7-cover.jpg",
              "locations": [
                  {
                      "type": "Point",
                      "coordinates": [
                          -122.479887,
                          38.510312
                      ],
                      "_id": "5c88fa8cf4afda39709c296f",
                      "day": 1
                  },
                  {
                      "type": "Point",
                      "coordinates": [
                          -122.582948,
                          38.585707
                      ],
                      "_id": "5c88fa8cf4afda39709c296e",
                      "day": 3
                  },
                  {
                      "type": "Point",
                      "coordinates": [
                          -122.434697,
                          38.482181
                      ],
                      "_id": "5c88fa8cf4afda39709c296d",
                      "day": 5
                  }
              ],
              "slug": "the-wine-taster",
              "durationWeeks": 0.7142857142857143,
              "id": "5c88fa8cf4afda39709c296c"
          }
      ]
  }
}



describe('shows the list of tour', () => {
  jest.setTimeout(10000);  
  let mockedReq;

  beforeEach(() => {
      mockedReq = {
          query: {}
      };
  })

  it("return list of tours", async () => {
    Tour.find = jest.fn().mockResolvedValue(mockGetAllTours); 
    
    const res = mockResponse() 
    await tourController.getAllTours(mockedReq,res)
    expect(Tour.find).toHaveBeenCalledTimes(1)
    expect(res.status).toHaveBeenCalledWith(200);
  });
});

let mockcreateTour = {
    "status": "success",
    "data": {
        "data": {
            "startLocation": {
                "type": "Point",
                "description": "Miami, USA",
                "coordinates": [
                    -80.185942,
                    25.774772
                ],
                "address": "301 Biscayne Blvd, Miami, FL 33132, USA"
            },
            "ratingsAverage": 4.8,
            "ratingsQuantity": 6,
            "images": [
                "tour-2-1.jpg",
                "tour-2-2.jpg",
                "tour-2-3.jpg"
            ],
            "createdAt": "2021-08-13T05:29:45.971Z",
            "startDates": [
                "2021-06-19T09:00:00.000Z",
                "2021-07-20T09:00:00.000Z",
                "2021-08-18T09:00:00.000Z"
            ],
            "secretTour": false,
            "guides": [
                "5c8a22c62f8fb814b56fa18b",
                "5c8a1f4e2f8fb814b56fa185"
            ],
            "_id": "611603692d354049d4ba26b9",
            "name": "Test tour 2",
            "duration": 7,
            "maxGroupSize": 15,
            "difficulty": "medium",
            "price": 497,
            "summary": "Exploring the jaw-dropping US east coast by foot and by boat",
            "description": "Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\nIrure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            "imageCover": "tour-2-cover.jpg",
            "locations": [
                {
                    "type": "Point",
                    "coordinates": [
                        -80.128473,
                        25.781842
                    ],
                    "_id": "5c88fa8cf4afda39709c2959",
                    "day": 1
                },
                {
                    "type": "Point",
                    "coordinates": [
                        -80.647885,
                        24.909047
                    ],
                    "_id": "5c88fa8cf4afda39709c2958",
                    "day": 2
                },
                {
                    "type": "Point",
                    "coordinates": [
                        -81.0784,
                        24.707496
                    ],
                    "_id": "5c88fa8cf4afda39709c2957",
                    "day": 3
                },
                {
                    "type": "Point",
                    "coordinates": [
                        -81.768719,
                        24.552242
                    ],
                    "_id": "5c88fa8cf4afda39709c2956",
                    "day": 5
                }
            ],
            "slug": "test-tour-2",
            "__v": 0,
            "durationWeeks": 1,
            "id": "611603692d354049d4ba26b9"
        }
    }
}

describe('create tour', () => {
    jest.setTimeout(10000);  
  
    it("create tours", async () => {
        const req = mockRequest();
        const res = mockResponse() 
        req.body = createmockdata;
    
        Tour.create = jest.fn().mockResolvedValue(mockcreateTour);
        await tourController.createTour(req.body,res)  

        expect(res.status).toHaveBeenCalledWith(201);
    });
  });
  
  let mockupdateTour = {
    "status": "success",
    "data": {
        "tour": {
            "startLocation": {
                "type": "Point",
                "description": "Banff, CAN",
                "coordinates": [
                    -115.570154,
                    51.178456
                ],
                "address": "224 Banff Ave, Banff, AB, Canada"
            },
            "ratingsAverage": 5,
            "ratingsQuantity": 9,
            "images": [
                "tour-1-1.jpg",
                "tour-1-2.jpg",
                "tour-1-3.jpg"
            ],
            "startDates": [
                "2021-04-25T09:00:00.000Z",
                "2021-07-20T09:00:00.000Z",
                "2021-10-05T09:00:00.000Z"
            ],
            "secretTour": false,
            "guides": [],
            "_id": "5c88fa8cf4afda39709c2951",
            "name": "The Forest Hiker",
            "duration": 5,
            "maxGroupSize": 25,
            "difficulty": "easy",
            "price": 397,
            "summary": "Breathtaking hike through the Canadian Banff National Park",
            "description": "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "imageCover": "tour-1-cover.jpg",
            "locations": [
                {
                    "type": "Point",
                    "coordinates": [
                        -116.214531,
                        51.417611
                    ],
                    "_id": "5c88fa8cf4afda39709c2954",
                    "day": 1
                },
                {
                    "type": "Point",
                    "coordinates": [
                        -118.076152,
                        52.875223
                    ],
                    "_id": "5c88fa8cf4afda39709c2953",
                    "day": 3
                },
                {
                    "type": "Point",
                    "coordinates": [
                        -117.490309,
                        51.261937
                    ],
                    "_id": "5c88fa8cf4afda39709c2952",
                    "day": 5
                }
            ],
            "slug": "the-forest-hiker",
            "__v": 0,
            "durationWeeks": 0.7142857142857143,
            "id": "5c88fa8cf4afda39709c2951"
        }
    }
}

describe('update tour', () => {
    jest.setTimeout(10000);  
  
    it("update tours by id", async () => {
        const req = mockRequest();
        const res = mockResponse() 
        req.params = "5c88fa8cf4afda39709c2951"
        req.body = updatemockdata;
    
        Tour.findByIdAndUpdate = jest.fn().mockResolvedValue(mockupdateTour);
        await tourController.updateTour(req,res)  
        expect(Tour.findByIdAndUpdate).toHaveBeenCalledTimes(1)
        expect(res.status).toHaveBeenCalledWith(200);
    });

    it("should not update data of tour when id param is provided wrong", async () => {
        const req = mockRequest();
        const res = mockResponse() 
        req.params = "5c88fa8cf4afda39709c2957"
        req.body = updatemockdata;
    
        Tour.findByIdAndUpdate = jest.fn().mockResolvedValue(null);
        await tourController.updateTour(req,res)
        let error = new AppError('No tour found with that id', 404);
        
       expect(error.statusCode).toBe(404);
       expect(error.status).toBe('fail');
    });
  }); 

  const mockgetTourStats = {
    "startLocation": {
      "description": "Miami, USA",
      "type": "Point",
      "coordinates": [-80.185942, 25.774772],
      "address": "301 Biscayne Blvd, Miami, FL 33132, USA"
    },
    "ratingsAverage": 4.8,
    "ratingsQuantity": 6,
    "images": ["tour-2-1.jpg", "tour-2-2.jpg", "tour-2-3.jpg"],
    "startDates": [
      "2021-06-19T09:00:00.000Z",
      "2021-07-20T09:00:00.000Z",
      "2021-08-18T09:00:00.000Z"
    ],
    "name": "Test tour 10",
    "duration": 7,
    "maxGroupSize": 15,
    "difficulty": "medium",
    "guides": ["5c8a22c62f8fb814b56fa18b", "5c8a1f4e2f8fb814b56fa185"],
    "price": 497,
    "summary": "Exploring the jaw-dropping US east coast by foot and by boat",
    "description": "Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\nIrure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    "imageCover": "tour-2-cover.jpg",
    "locations": [
      {
        "_id": "5c88fa8cf4afda39709c2959",
        "description": "Lummus Park Beach",
        "type": "Point",
        "coordinates": [-80.128473, 25.781842],
        "day": 1
      },
      {
        "_id": "5c88fa8cf4afda39709c2958",
        "description": "Islamorada",
        "type": "Point",
        "coordinates": [-80.647885, 24.909047],
        "day": 2
      },
      {
        "_id": "5c88fa8cf4afda39709c2957",
        "description": "Sombrero Beach",
        "type": "Point",
        "coordinates": [-81.0784, 24.707496],
        "day": 3
      },
      {
        "_id": "5c88fa8cf4afda39709c2956",
        "description": "West Key",
        "type": "Point",
        "coordinates": [-81.768719, 24.552242],
        "day": 5
      }
    ]
  }

  
describe('show tour stats details', () => {
    jest.setTimeout(30000);
  
    it("show tour stats details", async () => {
        const req = mockRequest();
        const res = mockResponse() 
    
        Tour.aggregate = jest.fn().mockResolvedValue(mockgetTourStats);
        await tourController.getTourStats(req,res)  
        expect(Tour.aggregate).toHaveBeenCalledTimes(1)
        expect(res.status).toHaveBeenCalledWith(200);
    });

})

const mockmonthlytour = 
    {
        "status": "success",
        "data": {
            "plan": [
                {
                    "numTourStarts": 6,
                    "tours": [
                        "The Sea Explorer",
                        "The City Wanderer",
                        "Test tour 1",
                        "Test tour 2",
                        "Test tour 3",
                        "Test tour 9"
                    ],
                    "month": 6
                },
                {
                    "numTourStarts": 6,
                    "tours": [
                        "The Sea Explorer",
                        "The Park Camper",
                        "Test tour 1",
                        "Test tour 2",
                        "Test tour 3",
                        "Test tour 9"
                    ],
                    "month": 8
                },
                {
                    "numTourStarts": 1,
                    "tours": [
                        "The Northern Lights"
                    ],
                    "month": 12
                }
            ]
        }
    }


describe('monthly plan tour', () => {
    jest.setTimeout(30000);
  
    it("should return data of monthly tour when id year is provided", async () => {
        const req = mockRequest();
        const res = mockResponse() 
        req.params.year = "2021" *1 ;
    
        Tour.aggregate = jest.fn().mockResolvedValue(mockmonthlytour);
        await tourController.getMonthlyPlan(req,res)  
        expect(Tour.aggregate).toHaveBeenCalledTimes(1)
        expect(res.status).toHaveBeenCalledWith(200);
    });
  }); 

  const mockDeleteTour = {
    "status": "success",
    "data": {
        "tour": {
            "startLocation": {
                "type": "Point",
                "description": "Miami, USA",
                "coordinates": [
                    -80.185942,
                    25.774772
                ],
                "address": "301 Biscayne Blvd, Miami, FL 33132, USA"
            },
            "ratingsAverage": 4.8,
            "ratingsQuantity": 6,
            "images": [
                "tour-2-1.jpg",
                "tour-2-2.jpg",
                "tour-2-3.jpg"
            ],
            "createdAt": "2021-08-13T05:29:45.971Z",
            "startDates": [
                "2021-06-19T09:00:00.000Z",
                "2021-07-20T09:00:00.000Z",
                "2021-08-18T09:00:00.000Z"
            ],
            "secretTour": false,
            "guides": [
                {
                    "role": "lead-guide",
                    "_id": "5c8a22c62f8fb814b56fa18b",
                    "name": "Miyah Myles",
                    "email": "miyah@example.com",
                    "photo": "user-12.jpg"
                },
                {
                    "role": "guide",
                    "_id": "5c8a1f4e2f8fb814b56fa185",
                    "name": "Jennifer Hardy",
                    "email": "jennifer@example.com",
                    "photo": "user-6.jpg"
                }
            ],
            "_id": "611603692d354049d4ba26b9",
            "name": "Test tour 2",
            "duration": 7,
            "maxGroupSize": 15,
            "difficulty": "medium",
            "price": 497,
            "summary": "Exploring the jaw-dropping US east coast by foot and by boat",
            "description": "Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\nIrure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            "imageCover": "tour-2-cover.jpg",
            "locations": [
                {
                    "type": "Point",
                    "coordinates": [
                        -80.128473,
                        25.781842
                    ],
                    "_id": "5c88fa8cf4afda39709c2959",
                    "day": 1
                },
                {
                    "type": "Point",
                    "coordinates": [
                        -80.647885,
                        24.909047
                    ],
                    "_id": "5c88fa8cf4afda39709c2958",
                    "day": 2
                },
                {
                    "type": "Point",
                    "coordinates": [
                        -81.0784,
                        24.707496
                    ],
                    "_id": "5c88fa8cf4afda39709c2957",
                    "day": 3
                },
                {
                    "type": "Point",
                    "coordinates": [
                        -81.768719,
                        24.552242
                    ],
                    "_id": "5c88fa8cf4afda39709c2956",
                    "day": 5
                }
            ],
            "slug": "test-tour-2",
            "__v": 0,
            "durationWeeks": 1,
            "id": "611603692d354049d4ba26b9"
        }
    }
}

describe('delete tour', () => {
    jest.setTimeout(30000);
  
    it("should return deleted data of tour when id year is provided", async () => {
        const req = mockRequest();
        const res = mockResponse() 
        req.params.id = "611603692d354049d4ba26b9"
    
        Tour.findByIdAndDelete = jest.fn().mockResolvedValue(mockDeleteTour);
        await tourController.deleteTour(req,res)  
        expect(Tour.findByIdAndDelete).toHaveBeenCalledTimes(1)
        expect(res.status).toHaveBeenCalledWith(200);
    });
  }); 

