const authController = require('../controller/authController');
const User = require('../models/userModels');

const mockRequest = () => {
  const req = {}
  req.body = jest.fn().mockReturnValue(req)
  req.params = jest.fn().mockReturnValue(req)
  return req
}

const mockResponse = () => {
  const res = {}
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res
}

const mockCreateUser = {
  "status": "success",
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxMWEyZWNiYjVjMTBiNjJjNDRmNmZlNCIsImlhdCI6MTYyOTEwNTg2OCwiZXhwIjoxNjM2ODgxODY4fQ.Vi1wj6MlEk3C850C8lZKmMkoLjDd6pkTI7MszcIw7ZQ",
  "data": {
    "user": {
      "role": "user",
      "active": true,
      "_id": "611a2ecbb5c10b62c44f6fe4",
      "name": "user",
      "email": "apurvabisht02@gmail.com",
      "__v": 0
    }
  }
}

const mockSignToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxMWEyZWNiYjVjMTBiNjJjNDRmNmZlNCIsImlhdCI6MTYyOTEwNTg2OCwiZXhwIjoxNjM2ODgxODY4fQ.Vi1wj6MlEk3C850C8lZKmMkoLjDd6pkTI7MszcIw7ZQ"


describe('create user', () => {
  jest.setTimeout(100000000);

  it("should create a user  ", async () => {
    const req = mockRequest();
    const res = mockResponse()
    req.params.id = "5c8a1e1a2f8fb814b56fa182"
    req.body = {
      "name": "user",
      "email": "abhaybisht2234441@gmail.com",
      "password": "test12345678",
      "passwordConfirm": "test12345678"
    }

    User.create = jest.fn().mockResolvedValue(mockCreateUser)
    let user = mockCreateUser.data.user
    console.log(user)
    const mockCallback = jest.fn(x => 42 + x);
    authController.createSendToken([0, 1], mockCallback);

    // The mock function is called twice
    // expect(mockCallback.mock.calls.length).toBe(2);
    // authController.signup = jest.fn().mockResolvedValue(mockSignToken)
    authController.createSendToken = jest.fn().mockResolvedValue(mockSignToken)
    await authController.signin(req, res)
    expect(res.status).toHaveBeenCalledWith(201);
    //   expect(res.json).toEqual(mockGetUser)
  });
});