const userController = require('../controller/usercontroller');
const User = require('../models/userModels');
const AppError = require('../utils/appError');

const mockRequest = () => {
    const req = {}
    req.body = jest.fn().mockReturnValue(req)
    req.params = jest.fn().mockReturnValue(req)
    return req
  }

const mockResponse = () => {
  const res = {}
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res
}

const mockGetUser = {
    "status": "success",
    "data": {
        "doc": {
            "role": "user",
            "_id": "5c8a1e1a2f8fb814b56fa182",
            "name": "Sophie Louise Hart",
            "email": "sophie@example.com",
            "photo": "user-3.jpg",
            "__v": 0
        }
    }
}

describe('shows the details of user by user id', () => {
    jest.setTimeout(100000000);
  
    it("should return data of user when id param is provided  ", async () => {
      const req = mockRequest();
      const res = mockResponse() 
      req.params.id = "5c8a1e1a2f8fb814b56fa182"
      
      User.findById = jest.fn().mockResolvedValue(mockGetUser) 
  
      await userController.getUser(req,res)
      expect(User.findById).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledWith(200);
    //   expect(res.json).toHaveBeenCalledWith(mockGetUser);
    });

    it("should not return data of user when id param is provided wrong  ", async () => {
        const req = mockRequest();
        const res = mockResponse() 
        req.params.id = "5c8a1e1a2f8fb814b56fa182"
        
        User.findById = jest.fn().mockResolvedValue(null) 
    
        await userController.getUser(req,res)
        let error = new AppError('No tour found with that id', 404);

       expect(error.statusCode).toBe(404);
       expect(error.status).toBe('fail');
      });
  
  });

  const mockGetAllUsers = {
    "status": "success",
    "results": 2,
    "data": {
        "data": [
            {
                "role": "user",
                "_id": "5c8a211f2f8fb814b56fa188",
                "name": "Cristian Vega",
                "email": "chris@example.com",
                "photo": "user-9.jpg"
            },
            {
                "role": "user",
                "_id": "6100eda0a5630831e4aa1cf6",
                "name": "user",
                "email": "apurvabisht@gmail.com",
                "passwordChangedAt": "2021-07-28T05:41:43.245Z"
            }
        ]
    }
}

describe('shows the details of user', () => {
    jest.setTimeout(100000000);
  
    it("should return list of user's details", async () => {
      const req = mockRequest();
      const res = mockResponse() 
      
      
      User.find = jest.fn().mockResolvedValue(mockGetAllUsers) 
  
      await userController.getAllUsers(req,res)
      expect(User.find).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledWith(200);
    });
})

let mockUpdateUser = {
    "status": "success",
    "data": {
        "data": {
            "role": "user",
            "_id": "6100eda0a5630831e4aa1cf6",
            "name": "Aliana",
            "email": "apurvabisht@gmail.com",
            "__v": 0,
            "passwordChangedAt": "2021-07-28T05:41:43.245Z"
        }
    }
}


describe('shows the updated user ', () => {
    jest.setTimeout(100000000);
  
    it("should return update details of user when id is provided", async () => {
      const req = mockRequest();
      const res = mockResponse() 
      
      
      User.findByIdAndUpdate = jest.fn().mockResolvedValue(mockUpdateUser) 
  
      await userController.updateUser(req,res)
      expect(User.findByIdAndUpdate).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledWith(200);
    });
})


const 
mockDeleteUser = {
    "status": "success",
    "data": {
        "doc": {
            "role": "lead-guide",
            "active": true,
            "_id": "5c8a21d02f8fb814b56fa189",
            "name": "Steve T. Scaife",
            "email": "steve@example.com",
            "photo": "user-10.jpg",
            "password": "$2a$12$CokDfXtt8quyqSQJVAJmAuXRces3.IS4er1TyN6O6tyr0NKjZJ1h2",
            "__v": 0
        }
    }
}



describe('delete user ', () => {
    jest.setTimeout(100000000);
  
    it("should delete user when id is provided", async () => {
      const req = mockRequest();
      const res = mockResponse() 
      req.params.id = "5c8a1dfa2f8fb814b56fa181"
      
      
      User.findByIdAndDelete = jest.fn().mockResolvedValue(mockDeleteUser) 
  
      await userController.deleteUser(req,res)
      expect(User.findByIdAndUpdate).toHaveBeenCalledTimes(1)
      expect(res.status).toHaveBeenCalledWith(200);
    });
})


const mockUpdateMe = {
    "status": "success",
    "data": {
        "user": {
            "role": "admin",
            "_id": "5c8a1d5b0190b214360dc057",
            "name": "Elina",
            "email": "elina@example.com",
            "photo": "user-1.jpg",
            "__v": 0,
            "passwordChangedAt": "2021-07-28T05:45:30.476Z"
        }
    }
}


describe('update user ', () => {
    jest.setTimeout(100000000);
  
    it("should return data of updated user", async () => {
      const req = mockRequest();
      const res = mockResponse() 
      req.params.body = {
        "name":"Elina",
        "email":"elina@example.com"
    }

    User.findByIdAndDelete = jest.fn().mockResolvedValue(mockDeleteUser) 
  
    await userController.updateMe(req,res)
    expect(res.status).toHaveBeenCalledWith(200);
    });
})

