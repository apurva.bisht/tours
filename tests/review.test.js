const reviewController = require('../controller/reviewcontroller');
const Review = require('../models/reviewModels');
const createmockdata = require("../tests/mock-data/review-data.json")


const mockRequest = () => {
    const req = {}
    req.body = jest.fn().mockReturnValue(req)
    req.params = jest.fn().mockReturnValue(req)
    return req
}

const mockResponse = () => {
    const res = {}
    res.status = jest.fn().mockReturnValue(res);
    res.json = jest.fn().mockReturnValue(res);
    return res
}

const mockGetAllReviews = {
    "status": "success",
    "results": 60,
    "data": {
        "data": [
            {
                "_id": "5c8a35b614eb5c17645c910b",
                "review": "Habitasse scelerisque class quam primis convallis integer eros congue nulla proin nam faucibus parturient.",
                "rating": 4,
                "user": {
                    "_id": "5c8a1dfa2f8fb814b56fa181",
                    "name": "Lourdes Browning",
                    "photo": "user-2.jpg"
                },
                "tour": "5c88fa8cf4afda39709c296c",
                "createdAt": "2021-07-28T05:18:48.038Z",
                "id": "5c8a35b614eb5c17645c910b"
            },
            {
                "_id": "5c8a34ed14eb5c17645c9108",
                "review": "Cras mollis nisi parturient mi nec aliquet suspendisse sagittis eros condimentum scelerisque taciti mattis praesent feugiat eu nascetur a tincidunt",
                "rating": 5,
                "user": {
                    "_id": "5c8a1dfa2f8fb814b56fa181",
                    "name": "Lourdes Browning",
                    "photo": "user-2.jpg"
                },
                "tour": "5c88fa8cf4afda39709c2955",
                "createdAt": "2021-07-28T05:18:48.035Z",
                "id": "5c8a34ed14eb5c17645c9108"
            }
        ]
    }
}

let mockGetReviews = {
    "status": "success",
    "data": {
        "doc": {
            "_id": "5c8a355b14eb5c17645c9109",
            "review": "Tempus curabitur faucibus auctor bibendum duis gravida tincidunt litora himenaeos facilisis vivamus vehicula potenti semper fusce suspendisse sagittis!",
            "rating": 4,
            "user": {
                "_id": "5c8a1dfa2f8fb814b56fa181",
                "name": "Lourdes Browning",
                "photo": "user-2.jpg"
            },
            "tour": "5c88fa8cf4afda39709c295a",
            "createdAt": "2021-07-28T05:18:48.036Z",
            "__v": 0,
            "id": "5c8a355b14eb5c17645c9109"
        }
    }
}


describe('shows the details of tour review', () => {
    jest.setTimeout(100000000);

    it("should return review of all tour ", async () => {
        const req = mockRequest();
        const res = mockResponse()

        Review.find = jest.fn().mockResolvedValue(mockGetAllReviews)

        await reviewController.getAllReviews(req, res)
        expect(Review.find).toHaveBeenCalledTimes(1)
        expect(res.status).toHaveBeenCalledWith(200);
    });

    it('should  return review of tour when id param is provided ', async () => {
        const req = mockRequest();
        const res = mockResponse()

        req.params.id = "5c8a355b14eb5c17645c9109";

        Review.find = jest.fn().mockResolvedValue(mockGetReviews)
        await reviewController.getAllReviews(req, res)
        expect(Review.find).toHaveBeenCalledTimes(1)
        expect(res.status).toHaveBeenCalledWith(200);

    });

});


const mockCreateReviewTour = {
    "status": "success",
    "data": {
        "review": {
            "_id": "611a0edcb045c73658d48733",
            "review": "Nice tour",
            "tour": "6116435e3653e44464650b2e",
            "user": "5c8a1e1a2f8fb814b56fa182",
            "createdAt": "2021-08-16T07:08:12.903Z",
            "__v": 0,
            "id": "611a0edcb045c73658d48733"
        }
    }
}


describe('create tour review', () => {
    jest.setTimeout(100000000);

    it("should create review for tour ", async () => {
        const req = mockRequest();
        const res = mockResponse()
        req.body = createmockdata;

        Review.create = jest.fn().mockResolvedValue(mockCreateReviewTour)

        await reviewController.createReview(req, res)
        expect(Review.create).toHaveBeenCalledTimes(1)
        expect(res.status).toHaveBeenCalledWith(200);
    });

});

const mockUpdateReviewTour = {
    "status": "success",
    "data": {
        "data": {
            "_id": "5c8a355b14eb5c17645c9109",
            "review": "Tempus curabitur faucibus auctor bibendum duis gravida tincidunt litora himenaeos facilisis vivamus vehicula potenti semper fusce suspendisse sagittis!",
            "rating": 4,
            "user": {
                "_id": "5c8a1dfa2f8fb814b56fa181",
                "name": "Lourdes Browning",
                "photo": "user-2.jpg"
            },
            "tour": "5c88fa8cf4afda39709c295a",
            "createdAt": "2021-07-28T05:18:48.036Z",
            "__v": 0,
            "id": "5c8a355b14eb5c17645c9109"
        }
    }
}


describe('update tour review', () => {
    jest.setTimeout(100000000);

    it("should update review for tour ", async () => {
        const req = mockRequest();
        const res = mockResponse()
        req.body = {
            "rating": 4
        }

        Review.findByIdAndUpdate = jest.fn().mockResolvedValue(mockUpdateReviewTour)

        await reviewController.updateReview(req, res)
        expect(Review.findByIdAndUpdate).toHaveBeenCalledTimes(1)
        expect(res.status).toHaveBeenCalledWith(200);
    });

});

const mockDeleteReview =
{
    "status": "success",
    "data": {
        "doc": {
            "_id": "5c8a359914eb5c17645c910a",
            "review": "Convallis turpis porttitor sapien ad urna efficitur dui vivamus in praesent nulla hac non potenti!",
            "rating": 5,
            "user": {
                "_id": "5c8a1dfa2f8fb814b56fa181",
                "name": "Lourdes Browning",
                "photo": "user-2.jpg"
            },
            "tour": "5c88fa8cf4afda39709c295d",
            "createdAt": "2021-07-28T05:18:48.037Z",
            "__v": 0,
            "id": "5c8a359914eb5c17645c910a"
        }
    }
}
describe('delete tour review', () => {
    jest.setTimeout(100000000);

    it("should delete review for tour  ", async () => {
        const req = mockRequest();
        const res = mockResponse()
        req.params.id = "5c8a364c14eb5c17645c910c"

        Review.findByIdAndDelete = jest.fn().mockResolvedValue(mockDeleteReview)

        await reviewController.deleteReview(req, res)
        expect(Review.findByIdAndUpdate).toHaveBeenCalledTimes(1)
        expect(res.status).toHaveBeenCalledWith(200);
    });

});